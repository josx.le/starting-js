// console.log("Hola mundo")

// var x = 90
// let y = 89
// console.log(x * y)
// x = true
// y = "foo"

// console.log(x, y)

// var array = [1, 2, 3, 4, 5, "foo", "bar", 2, 3, 4, 5, true, false]
// var z;
// console.log(typeof array, typeof z )

// console.log(array[6])

// var obj = {};
// obj = {
//     first_name: "foo",
//     last_name_: "bar",
//     age: 23,
//     city: "TJ",
//     status: true
// }

// console.log(obj.age)

// for (let i = 0; i < 100; i+=5) {
//     console.log[i]
// }

// for (let i = 0; i < array.length; i++) {
//     console.log(array[i])
// }

// for (let i of array) {
//     console.log[i]
// }

// string = " foo bar"
// for ( let c of string) {
//     console.log[c]
// }

// for (let key of Object.keys(obj)) {
//     console.log[" Key" + key + " " + "value: " + obj[key]]
// }

// --do while una ves minimo
// --while puede no ejecutarse

// var i = 0

// while(i < 1000) {
//     console.log(i)
//     i += 10
// }

// do {
//     console.log(i)
//     i += 1000
// } while(i <= 10000)


// var a = 'Hola';

// function  foo() {
//     var b = 'Mundo';

//     function bar() {
//         var c = '!!';

//         console.log(a);
//         console.log(b);
//         console.log(c);
//     }
//     bar()
// }

// console.log(foo())

/*PRIMER EJERCICIO*/

// var a = 'Hola';

// function foo() {
//     var a = 'Mundo';

//     function bar() {
//         console.log(a);
//     }
//     bar()
// }

// console.log(foo())

/*SEGUNDO EJERCICIO */

// function foo() {
//     const a = true;

//     function bar() {
//         const a = false;
//         console.log(a);
//     }
//     bar()
// }
// console.log(foo())

/*TERCER EJERCICIO */

// var prism = function(l, w , h) {
//     return l * w * h;
// }

// function prism(l) {
//     return function(w) {
//         return function(h) {
//             return l *w * h;
//         }
//     }
// }

// console.log(prism(l)(w)(h))

// var prism = l => w => h => l * w * h;

// var fun = (function(msg) {
//     console.log("I'am" + msg)
// })(' god');

// var example = (function() {
//     return 101;
//     }());
//     console.log(example);

// var foo = function sum (x, y) {
//     return x + y
// }

// console.log(foo(5, 10))

// //sum(5, 10) NO DESCOMENTAR

// function bar (x, y) {
//     return x + y
// }

// console.log(bar(5, 10))

/* RECURSIVIDAD */

// var say = function say (times) {
//     if (times > 0 ) {
//         console.log("hello")
//         say(times - 1)
//     } 
// }

// say(10)

/*TRUENA POR QUE LA FUNCION NO TIENE NOMBRE, SI LE PONES NOMBRE DEJA DE TRINAR*/

// var saysay = say
// say = "oops!"
// saysay(10)

/**Functions with an Unknown Number of Arguments */

// function personLogsSomeThings(person, ...msg) {
//     msg.forEach(arg => {
//     console.log(person, 'says', arg);
//     });
//     }
//     personLogsSomeThings('John', 'hello', 'world');

//     var nums = [0,1,2];
//     var doubledNums = nums.map( function(element){ return element * 10; } );

//     console.log(doubledNums)


    // function sum(x = 5, y = 500) {
    //     return x + y
    // }

    // console.log(sum())

    /*FETCH */

    /*FOR OF SIRVE PARA RECORRER ARRAYS */

// var url = "http://api.stackexchange.com/2.2/questions?site=stackoverflow&tagged=javascript"

// const responseData = fetch(url).then(response => response.json());
// responseData.then(({items}) => {
//     for(const {title} of items) {
//         console.log("Questions Title: " + title + "?")
//     }
// })

const url = "https://jsonplaceholder.typicode.com/users"

fetch(url, {
    method: "POST",
    headers: {
        "Content_type": "aplications/json"
    },
    body: JSON.stringify({
        username: "Foo",
        email: "foo@foo.com"
    })
}).then (response => response.json())
.then(response => console.log(response))