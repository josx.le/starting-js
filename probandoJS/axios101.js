const axios = require('axios')

const url = "https://jsonplaceholder.typicode.com/users"

// axios.get(url).then(({data}) => {
//     data.forEach(element => {
//         console.log("UserID: " + element.id
//             +   "Username: " + element.username)
//     });
// })

axios.post(url, {
    username: "Foo",
    email: "foo@foo.com"
}).then(({data})=> console.log(data))